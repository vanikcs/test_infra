data "vkcs_networking_network" "extnet" {
  name="ext-net"
}

resource "vkcs_networking_network" "compute-user10" {
  name="compute net-user10"
}

resource "vkcs_networking_subnet" "compute-user10" {
  name="subnet_1-user10"
  network_id = vkcs_networking_network.compute-user10.id
  cidr="192.168.0.0/24"
  ip_version=4
}

resource "vkcs_networking_router" "compute-user10" {
  name="vm-router-user10"
  admin_state_up = true
  external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "compute-user10" {
  router_id = vkcs_networking_router.compute-user10.id
  subnet_id = vkcs_networking_subnet.compute-user10.id
}

data "vkcs_compute_flavor" "compute" {
  name="Basic-1-2-20"
}

data "vkcs_images_image" "compute" {
  name="Debian11.4.202208"
  }

resource "vkcs_compute_keypair" "test-keypair-user10" {
  name       = "user10-terraform"
  public_key = file("./keys/id_rsa.pub")
}

resource "vkcs_compute_instance" "user10-exam" {
  name="user10-exam"
  flavor_id = data.vkcs_compute_flavor.compute.id
  security_groups = ["default","all"]
  availability_zone = "GZ1"
  key_pair = vkcs_compute_keypair.test-keypair-user10.name

  block_device {
    uuid = data.vkcs_images_image.compute.id
    source_type="image"
    destination_type = "volume"
    volume_type = "ceph-ssd"
    volume_size = 8
    boot_index = 0
    delete_on_termination = true
  }

  block_device {
    source_type="blank"
    destination_type = "volume"
    volume_type = "ceph-ssd"
    volume_size = 8
    boot_index = 1
    delete_on_termination = true
  }

  network {
    uuid = vkcs_networking_network.compute-user10.id
  }

  depends_on = [
    vkcs_networking_network.compute-user10,
    vkcs_networking_subnet.compute-user10
  ]
}

resource "vkcs_networking_floatingip" "fip" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip" {
  floating_ip = vkcs_networking_floatingip.fip.address
  instance_id = vkcs_compute_instance.user10-exam.id
}

output "instance_fip" {
  value = vkcs_networking_floatingip.fip.address
}

data "template_file" "inventory" {
  template = file("inventory.tpl")

  vars = {
    host = vkcs_networking_floatingip.fip.address
	}

}

resource "local_file" "save_inventory" {
  content  = data.template_file.inventory.rendered
  filename = "../ansible/inventory"
}
