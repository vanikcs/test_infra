[deb]
srv ansible_host=${host} ansible_user=debian

[all:vars]
ansible_ssh_private_key_file=../terraform/keys/id_rsa
