#!/bin/bash
cd terraform
cp ./terraformrc $HOME/.terrafomrc
echo "================="
echo "Initing providers"
echo "================="
sudo terraform init > /dev/null
echo "===================="
echo "Making infrastruture"
echo "===================="
sudo terraform apply -auto-approve > /dev/null
cd ../ansible
echo "====================="
echo "Deploying application"
echo "====================="
sudo ansible-playbook playbook.yml > /dev/null
IP_APP_HOST=`sudo ansible-playbook playbook.yml --start-at-task "Show host ip" | grep msg | awk '{print $2}' | tr -d \"`
echo "======================================="
echo "Your app url: http://$IP_APP_HOST"
echo "======================================="
