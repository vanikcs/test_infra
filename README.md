# Stack

NGINX (Reverse Proxy + Loadbalancer) + Uvicorn(FastAPI) + Postgresql

# Run

- Generate ssh key and copy public and private keys to `./terraform/keys`;
- Create and set `provider.tf`;

```
terraform {
    required_providers {
        vkcs = {
            source = "vk-cs/vkcs"
            version = "~> 0.1.12"
        }

        template = {
            source = "hashicorp/template"
            version = "~> 2.2.0"
        }

        local = {
            source = "hashicorp/local"
            version = "~> 2.2.3"
        }
    }
}

provider "vkcs" {
    username = ""
    password = ""
    project_id = ""
    region = "RegionOne"
    auth_url = "https://infra.mail.ru:35357/v3/"
}

provider "template" {

}

provider "local" {

}
```

### Automatic

- Run `./run-me.yml`;

### Manual

- Create and set `.terraformrc` in `$HOME`;
- Initialize provider `terraform init`;
- Create infrastructure `terraform apply`;
- Run ansible-playbook `playbook.yml`;
